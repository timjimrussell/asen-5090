function [health, pos] = broadcast_eph2pos(ephem, weektime, prn)

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Size time and position
[num_meas, ~ ] = size(weektime);
health = zeros(num_meas,1);
pos = zeros(num_meas,3);

% Filter and size ephemeris by PRN number
ephem_filtered = ephem(find(ephem(:,1)==prn),:);
[num_entries, ~] = size(ephem_filtered);

for i = 1:num_meas
    % Grab correct ephemeris entry
    use_index = 0;
    week = weektime(i,1);
    tow = weektime(i,2);
    for j = 1:num_entries
        if tow >= ephem_filtered(j,17)
            use_index = j;
        else
            break
        end
    end
    % In case time stamp is before first entry, use first entry and warn
    if use_index == 0
        use_index = 1;
        fprintf('WARNING: Time stamp comes before first ephem entry!\n')
    end

    % Define constants
    mu = 3.986005e14; % m^3/s^2 - WGS84 standard gravitational parameter
    Omdot_e = 7.2921151467e-5; % rad/s - WGS84 Earth rotation rate

    % Calculate mean motion
    a = ephem_filtered(use_index,5)^2; % m - semi-major axis
    n0 = sqrt(mu/a^3); % rad/sec - mean motion

    % Calculate time from reference
    t_e = ephem_filtered(use_index,17); % s - time into week at ephem entry
    t_j = tow - t_e; % s - time into week at measurement

    % Calculate corrected mean motion
    deln = ephem_filtered(use_index,3); % rad/s - mean motion correction
    n = n0 + deln; % rad/s - corrected mean motion

    % Calculate mean anomaly
    M_0 = ephem_filtered(use_index,2); % rad - mean anomaly at ephem entry
    M_j = M_0 + n*t_j; % rad - mean anomaly at measurement

    % Calculate eccentric anomaly
    e = ephem_filtered(use_index,4); % unitless - eccentricity of orbit
    %E_j = newton_kepler_solver(M_j,a,e,1e-6); % rad - eccentric anomaly at measurement
    E_j = newton_kepler_solver(M_j, a, e, 1e-2);
    
    % Calculate true anomaly
    theta_j = atan2(sqrt(1-e^2)*sin(E_j),cos(E_j)-e);

    % Calculate corrected argument of latitude
    om = ephem_filtered(use_index,8); % rad - argument of perigee
    phi_j = theta_j + om; % rad - argument of latitude
    C_us = ephem_filtered(use_index,12); % unitless - amplitude of the sine harmonic correction term to the argument of latitude
    C_uc = ephem_filtered(use_index,11); % unitless - amplitude of the cosine harmonic correction term to the argument of latitude
    delu_j = C_us*sin(2*phi_j) + C_uc*cos(2*phi_j); % rad - argument of latitude correction
    u_j = phi_j + delu_j; % rad - corrected argument of latitude

    % Calculate radius
    C_rs = ephem_filtered(use_index,14); % unitless - amplitude of the sine harmonic correction term to the orbit radius
    C_rc = ephem_filtered(use_index,13); % unitless - amplitude of the cosine harmonic correction term to the orbit radius
    delr_j = C_rs*sin(2*phi_j) + C_rc*cos(2*phi_j); % rad - radius correction
    r_j = a*(1-e*cos(E_j)) + delr_j; % m - corrected radius

    % Calculate inclination
    i_0 = ephem_filtered(use_index,7); % rad - inclination at ephem entry
    idot = ephem_filtered(use_index,10); % rad/s - inclination rate
    C_is = ephem_filtered(use_index,16); % unitless - amplitude of the sine harmonic correction term to the angle of inclination
    C_ic = ephem_filtered(use_index,15); % unitless - amplitude of the cosine harmonic correction term to the angle of inclination
    deli_j = C_is*sin(2*phi_j) + C_ic*cos(2*phi_j); % rad - inclination correction
    i_j = i_0 + deli_j + idot*t_j; % rad - inclination at measurement

    % Calculate coordinates in orbit plane
    x_j = r_j*cos(u_j);
    y_j = r_j*sin(u_j);

    % Calculate longitude of ascending node
    Om_0 = ephem_filtered(use_index,6); % rad - longitude of ascending node of orbit plane at weekly epoch
    Omdot = ephem_filtered(use_index,9); % rad/s - rate of longitude of ascending node
    Om_j = Om_0 + (Omdot - Omdot_e)*t_j - Omdot_e*t_e;

    % Calculate ECEF coordinates
    x_ecef = x_j*cos(Om_j) - y_j*cos(i_j)*sin(Om_j);
    y_ecef = x_j*sin(Om_j) + y_j*cos(i_j)*cos(Om_j);
    z_ecef = y_j*sin(i_j);
    pos(i,:) = [x_ecef y_ecef z_ecef];
    
    % Populate health array
    health(i,1) = ephem_filtered(use_index,25);
end
