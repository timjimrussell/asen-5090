%--------------------------------------------------------------------------
%
%  PROCEDURE NAME: 
%      newton_kepler_solver.m
%
%  PROCEDURE DESCRIPTION:
%      Solves Kepler's equation for eccentric anomaly of an orbit to within 
%      a defined spacecraft position uncertainty using Newton's method for 
%      finding roots of a function.
%
%  ALGORITHM:
%      1. Calculate orbit period and normalize T to within 0 <= T < P.
%      2. Calculate mean motion based on orbit parameters.
%      3. Calculate mean anomaly at given time past periapsis.
%      4. Define first guess of eccentric anomaly E = M.
%      5. Subtract mean anomaly from Kepler's equation evaluated for the
%         current eccentric anomaly guess.
%      6. Calculate maximum eccentric anomaly uncertainty using the arc
%         length formula with the value calculated in Step 6 as the radius
%         and the user-defined position uncertainty as the arc length.
%      7. Record current eccentric anomaly guess, maximum eccentricity
%         uncertainty calculated in Step 6, and difference calculated
%         in Step 5.
%      8. If the magnitude of the difference calculated in Step 6 is less 
%         than the user-defined tolerance or if 100 iterations are reached:
%         a) Proceed. 
%         Else: 
%         b) Update the eccentric anomaly guess as the current guess minus 
%            the ratio of the current calculated difference and the
%            derivative of Kepler's equation evaluated at the current 
%            guess. 
%         c) Go to Step 5.
%      9. Return calculated value for eccentric anomaly and guess history.
%
%  AUTHOR:
%      Timothy Russell, ProMS Student, CU Boulder
%      timothy.russell@colorado.edu
%
%  INPUTS:
%    T       - Time on orbit since periapsis (s)
%    a       - Semi-major axis of the orbit (km)
%    e       - Eccentricity of the orbit (unitless)
%    mu      - Gravitational parameter of the two bodies (km^3/s^2)
%    delr    - Position uncertainty of satellite (km)
%
%  OUTPUTS:
%    E       - Eccentric anomaly calculated at T (rad)
%
%  LOCALS:
%    n       - Mean motion of orbit (rad/s)
%    M       - Mean anomaly calculated at T (rad)
%    P       - Orbit period (s)
%    r       - Position calculated for E (km)
%    delE    - Maximum allowable uncertainty in eccentric anomaly in order
%              to meet position uncertainty requirement
%    diff    - Difference between current iteration of Kepler's equation
%              and mean anomaly (rad)
%    E_guess - Array of guesses for E and corresponding eccentric anomaly 
%              uncertainty and difference calculations ([rad rad])
%
%  PROCEDURES CALLS:
%
%  REFERENCES:
%    "Orbital Mechanics for Engineering Students, Second Edition"
%        Howard D. Curtis
%    "Fundamentals of Astrodynamics and Applications, Fourth Edition"
%        David A. Vallado
%    ASEN 5050 Fall 2019 Class Notes
%        Natasha Bosanac
%
%  VERSON HISTORY:
%    1.3    2020/10/01    Repurposed to work with different inputs
%    1.2    2019/09/24    Added maximum number of iterations and "sanity
%                         check"
%    1.1    2019/09/23    Changed tolerance to user-defined position
%                         uncertainty and added appropriate calculations 
%                         and checks
%    1.0    2019/09/22    First working version, hard-coded tolerance
%
%--------------------------------------------------------------------------

function E = newton_kepler_solver(M, a, e, delr)

% First guess E = M and evaluate quality of guess
E = M;
diff = E - e*sin(E) - M;

% Calculate tolerance for first guess
delE = abs(delr/(a*e*sin(E)));

% Log first guess results
E_guess = [E, delE, diff];

% Continue to iterate until tolerance is met
while (abs(diff) >= delE) && (length(E_guess(:,1)) ~= 100)
    E = E - diff/(1 - e*cos(E));
    diff = E - e*sin(E) - M;
    delE = abs(delr/(a*e*sin(E)));
end

% Tell user if max iterations reached
if (size(E_guess) == [100, 3])
    fprintf('WARNING: Maximum number of iterations reached!\n\n')
end
