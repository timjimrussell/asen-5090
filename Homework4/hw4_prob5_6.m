function hw4_prob5_6

close all

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Generate ephemeris data
ephem = read_clean_GPSbroadcast('brdc2450.20n');

% Build time variables
i = 86400*2;
weektimes = [];
while i <= 86400*3
    weektimes = [weektimes; 2121 i];
    i = i + 30;
end
hours = (weektimes(:,2)-86400*2)./3600;

% Define NIST ECEF coordinates
nist_ecef = [-1288398,-4721697,4078625];

%% PRN01

% Generate PRN01 ECEF data
[~, prn01_ecef] = broadcast_eph2pos(ephem, weektimes, 1);
[prn01_nist_az, prn01_nist_el, prn01_nist_range] = calc_azelrange(nist_ecef, prn01_ecef);

% Correct range for light travel time
prn01_nist_range_corrected = zeros(length(hours),1);
for i = 1:length(hours)
    prn01_nist_range_corrected(i,1) = correct_range(nist_ecef,prn01_ecef(i,:),ephem,weektimes(i,2),1);
end

% Plot azimuth
figure
plot(hours,prn01_nist_az)
xlim([0 24])
ylim([-180 180])
title('PRN01 Azimuth Angle from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Azimuth angle (deg)')

% Plot elevation
figure
plot(hours,prn01_nist_el)
xlim([0 24])
ylim([-90 90])
title('PRN01 Elevation Angle from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Elevation angle (deg)')

% Plot range and corrected range
figure
plot(hours,prn01_nist_range,'-')
hold on
plot(hours,prn01_nist_range_corrected,'--')
xlim([0 24])
title('PRN01 Range from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Range (m)')
legend('Uncorrected','Corrected')

% Plot range diffs
prn01_nist_range_diff = abs(prn01_nist_range - prn01_nist_range_corrected);
fprintf('Max range correction for PRN01: %f m.',max(prn01_nist_range_diff))
figure
plot(hours, prn01_nist_range_diff);
xlim([0 24])
title('PRN01 Range Corrections from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Range Correction (m)')

%% PRN05

% Generate PRN05 ECEF data
[~, prn05_ecef] = broadcast_eph2pos(ephem, weektimes, 5);
[prn05_nist_az, prn05_nist_el, prn05_nist_range] = calc_azelrange(nist_ecef, prn05_ecef);

% Correct range for light travel time
prn05_nist_range_corrected = zeros(length(hours),1);
for i = 1:length(hours)
    prn05_nist_range_corrected(i,1) = correct_range(nist_ecef,prn05_ecef(i,:),ephem,weektimes(i,2),5);
end

% Plot azimuth
figure
plot(hours,prn05_nist_az)
xlim([0 24])
ylim([-180 180])
title('PRN05 Azimuth Angle from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Azimuth angle (deg)')

% Plot elevation
figure
plot(hours,prn05_nist_el)
xlim([0 24])
ylim([-90 90])
title('PRN05 Elevation Angle from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Elevation angle (deg)')

% Plot range and corrected range
figure
plot(hours,prn05_nist_range,'-')
hold on
plot(hours,prn05_nist_range_corrected,'--')
xlim([0 24])
title('PRN05 Range from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Range (m)')
legend('Uncorrected','Corrected')

% Plot range diffs
prn05_nist_range_diff = abs(prn05_nist_range - prn05_nist_range_corrected);
fprintf('Max range correction for PRN05: %f m.',max(prn05_nist_range_diff))
figure
plot(hours, prn05_nist_range_diff);
xlim([0 24])
title('PRN05 Range Corrections from NIST IGS Site')
xlabel('Time into day (hr)')
ylabel('Range Correction (m)')
