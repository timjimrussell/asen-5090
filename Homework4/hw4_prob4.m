function hw4_prob4

close all

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Generate almanac and ephemeris data
[alm, ~] = read_GPSyuma('YUMA245.alm',2);
ephem = read_clean_GPSbroadcast('brdc2450.20n');

% Build time variables
i = 86400*2;
weektimes = [];
while i <= 86400*3
    weektimes = [weektimes; 2121 i];
    i = i + 30;
end
hours = (weektimes(:,2)-86400*2)./3600;

%% PRN01

% Generate PRN01 ECEF data
[~, ecef_prn01_ephem] = broadcast_eph2pos(ephem, weektimes, 1);
[~, ecef_prn01_alm] = broadcast2pos(alm, weektimes, 1);
ecef_prn01_diff = (ecef_prn01_ephem - ecef_prn01_alm);

% 3-D scatter plot
figure
plot3(ecef_prn01_ephem(:,1),ecef_prn01_ephem(:,2),ecef_prn01_ephem(:,3),'-')
hold on
plot3(ecef_prn01_alm(:,1),ecef_prn01_alm(:,2),ecef_prn01_alm(:,3),'--')
axis equal
title('PRN01 ECEF Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('ECEF X position (m)')
ylabel('ECEF Y position (m)')
zlabel('ECEF Z position (m)')

% ECEF X comparison
figure
plot(hours,ecef_prn01_ephem(:,1),'-')
hold on
plot(hours,ecef_prn01_alm(:,1),'-')
xlim([0 24])
title('PRN01 ECEF-X Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF X position (m)')

% ECEF Y comparison
figure
plot(hours,ecef_prn01_ephem(:,2),'-')
hold on
plot(hours,ecef_prn01_alm(:,2),'-')
xlim([0 24])
title('PRN01 ECEF-Y Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF Y position (m)')

% ECEF Z comparison
figure
plot(hours,ecef_prn01_ephem(:,3),'-')
hold on
plot(hours,ecef_prn01_alm(:,3),'-')
xlim([0 24])
title('PRN01 ECEF-Z Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF Y position (m)')

% Diff comparison
[foo, ~] = size(ecef_prn01_diff);
prn01_range_diff = zeros(foo,1);
for i = 1:foo
    prn01_range_diff(i,1) = norm(ecef_prn01_diff(i,:));
end
figure
plot(hours,ecef_prn01_diff(:,1),'-')
hold on
plot(hours,ecef_prn01_diff(:,2),'--')
plot(hours,ecef_prn01_diff(:,3),':k')
plot(hours, prn01_range_diff,'-.')
xlim([0 24])
title('PRN01 ECEF Coordinate Differences for 2020-09-01')
legend('X Coordinate', 'Y Coordinate', 'Z Coordinate', 'Total (RSS)')
xlabel('Time into day (hr)')
ylabel('Position difference (m)')

%% PRN05

% Generate PRN05 ECEF data
[~, ecef_prn05_ephem] = broadcast_eph2pos(ephem, weektimes, 5);
[~, ecef_prn05_alm] = broadcast2pos(alm, weektimes, 5);
ecef_prn05_diff = ecef_prn05_ephem - ecef_prn05_alm;

% 3-D scatter plot
figure
plot3(ecef_prn05_ephem(:,1),ecef_prn05_ephem(:,2),ecef_prn05_ephem(:,3),'-')
hold on
plot3(ecef_prn05_alm(:,1),ecef_prn05_alm(:,2),ecef_prn05_alm(:,3),'--')
axis equal
title('PRN05 ECEF Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('ECEF X position (m)')
ylabel('ECEF Y position (m)')
zlabel('ECEF Z position (m)')

% ECEF X comparison
figure
plot(hours,ecef_prn05_ephem(:,1),'-')
hold on
plot(hours,ecef_prn05_alm(:,1),'-')
xlim([0 24])
title('PRN05 ECEF-X Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF X position (m)')

% ECEF Y comparison
figure
plot(hours,ecef_prn05_ephem(:,2),'-')
hold on
plot(hours,ecef_prn05_alm(:,2),'-')
xlim([0 24])
title('PRN05 ECEF-Y Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF Y position (m)')

% ECEF Z comparison
figure
plot(hours,ecef_prn05_ephem(:,3),'-')
hold on
plot(hours,ecef_prn05_alm(:,3),'-')
xlim([0 24])
title('PRN05 ECEF-Z Coordinates for 2020-09-01')
legend('Broadcast Ephemeris', 'YUMA Almanac')
xlabel('Time into day (hr)')
ylabel('ECEF Y position (m)')

% Diff comparison
[bar, ~] = size(ecef_prn05_diff);
prn05_range_diff = zeros(bar,1);
for i = 1:bar
    prn05_range_diff(i,1) = norm(ecef_prn05_diff(i,:));
end
figure
plot(hours,ecef_prn05_diff(:,1),'-')
hold on
plot(hours,ecef_prn05_diff(:,2),'--')
plot(hours,ecef_prn05_diff(:,3),':k')
plot(hours, prn05_range_diff,'-.')
xlim([0 24])
title('PRN05 ECEF Coordinate Differences for 2020-09-01')
legend('X Coordinate', 'Y Coordinate', 'Z Coordinate', 'Total (RSS)')
xlabel('Time into day (hr)')
ylabel('Position difference (m)')
