function hw4_prob_7_8

close all

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Generate ephemeris data
ephem = read_clean_GPSbroadcast('brdc2450.20n');

% Define NIST ECEF coordinates
nist_ecef = [-1288398,-4721697,4078625];

%% PRN01

% Generate PRN01 RINEX pseudoranges
prn01_rinex = read_rinex_obs8('nist2450.20o',1);
prn01_rinex_weektimes = prn01_rinex.data(:,1:2);
prn01_rinex_hours = (prn01_rinex_weektimes(:,2)-86400*2)./3600;
prn01_c1_pseudorange = prn01_rinex.data(:,4);
prn01_p2_pseudorange = prn01_rinex.data(:,8);

% Generate PRN01 ephemeris predicted ranges
[~, prn01_ecef] = broadcast_eph2pos(ephem, prn01_rinex_weektimes, 1);
prn01_nist_range_corrected = zeros(length(hours),1);
for i = 1:length(prn01_rinex_hours)
    prn01_nist_range_corrected(i,1) = correct_range(nist_ecef,prn01_ecef(i,:),ephem,prn01_rinex_weektimes(i,2),1);
end

% Plot all ranges
figure
scatter(prn01_rinex_hours,prn01_nist_range_corrected,'.')
hold on
scatter(prn01_rinex_hours,prn01_c1_pseudorange,'.')
scatter(prn01_rinex_hours,prn01_p2_pseudorange,'.')

% Plot range diffs
prn01_pred_c1_diff = abs(prn01_nist_range_corrected - prn01_c1_pseudorange);
prn01_pred_p2_diff = abs(prn01_nist_range_corrected - prn01_p2_pseudorange);
figure
plot(prn01_rinex_hours,prn01_pred_c1_diff)
hold on
plot(prn01_rinex_hours,prn01_pred_p2_diff)

%% PRN05

% Generate PRN05 RINEX pseudoranges
prn05_rinex = read_rinex_obs8('nist2450.20o',5);
prn05_rinex_weektimes = prn05_rinex.data(:,1:2);
prn05_rinex_hours = (prn05_rinex_weektimes(:,2)-86400*2)./3600;
prn05_c1_pseudorange = prn05_rinex.data(:,4);
prn05_p2_pseudorange = prn05_rinex.data(:,8);

% Generate PRN05 ephemeris predicted ranges
[~, prn05_ecef] = broadcast_eph2pos(ephem, prn05_rinex_weektimes, 5);
prn05_nist_range_corrected = zeros(length(hours),1);
for i = 1:length(prn05_rinex_hours)
    prn05_nist_range_corrected(i,1) = correct_range(nist_ecef,prn05_ecef(i,:),ephem,prn05_rinex_weektimes(i,2),5);
end

% Plot all ranges
figure
plot(prn05_rinex_hours,prn05_nist_range_corrected,'.')
hold on
scatter(prn05_rinex_hours,prn05_c1_pseudorange,'.')
scatter(prn05_rinex_hours,prn05_p2_pseudorange,'.')

% Plot range diffs
prn05_pred_c1_diff = abs(prn05_nist_range_corrected - prn05_c1_pseudorange);
prn05_pred_p2_diff = abs(prn05_nist_range_corrected - prn05_p2_pseudorange);
figure
plot(prn05_rinex_hours,prn05_pred_c1_diff)
hold on
plot(prn05_rinex_hours,prn05_pred_p2_diff)