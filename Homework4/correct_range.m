function R_new = correct_range(user_ecef, sv_ecef, ephem, t_r, PRN)

% Define physical constants
c = 299792458; % m/s
om_E = 7292115.0e-11; % rad/s

r_GPS = sv_ecef.';
r_Rx = user_ecef.';
R_prev = norm(r_GPS - r_Rx);

should_loop = true;
while should_loop
    t_t = t_r - R_prev/c;
    [~, r_GPS] = broadcast_eph2pos(ephem,[ephem(1,19) t_t],PRN);
    r_GPS = r_GPS.';
    phi = om_E*(t_r - t_t);
    r_GPS = [cos(phi) sin(phi) 0; -sin(phi) cos(phi) 0; 0 0 1]*r_GPS;
    R_new = norm(r_GPS - r_Rx);
    if abs(R_new - R_prev) <= 1e-2
        should_loop = false;
    end
    R_prev = R_new;
end
