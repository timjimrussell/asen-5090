function site_skyplot(user_ecef, start_week, start_tow, num_mins)
%calc_los - Generate skyplots of GPS satellites for a given site
%
% Inputs:
%    user_ecef  - 1x3 float, location of user in ECEF coordinates in m
%    start_week - int, GPS week of first timestamp
%    start_tow  - int, time of week, in seconds, of first timestamp
%    num_mins   - int, number of minutes to propagate the skyplot
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

% Generate week/TOW pairs
times = zeros(num_mins+1, 2);
cur_week = start_week;
cur_tow = start_tow;
times(1,:) = [cur_week cur_tow];
for i = 2:num_mins+1
    if cur_tow + 60 >= (86400*7)
        cur_week = mod(cur_week+1,1024);
    end
    cur_tow = mod(cur_tow+60,86400*7);
    times(i,:) = [cur_week cur_tow];
end

[alm, ~] = read_GPSyuma('YUMA245.alm',2);
sv_array = alm(:,1).';
num_sv = length(sv_array);
az_array = [];
el_array = [];

for i = 1:(num_mins+1)
    for j = 1:num_sv
        [~, sv_ecef] = broadcast2pos(alm, times(i,:), sv_array(j));
        [az, el, ~] = calc_azelrange(user_ecef, sv_ecef);
        if el >= 0
            az_array = [az_array az];
            el_array = [el_array el];
        end
    end
end

svs = zeros(1,length(az_array));

figure
plotAzEl(az_array,el_array,svs)

%------------- END OF CODE --------------

