function coord_table = example_site_coords()
%example_site_coords - Generates a table of LLA and ECEF coordinates for:
%   1. NIST (Boulder, CO)
%   2. EQUA @ NIST longitude
%   3. 89NO @ NIST longitude
%
% Inputs:
%
% Outputs:
%    coord_table - 3x7 ECEF and LLA coordinates for each site
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

% Predefine NIST ECEF
nist_x = -1288398;
nist_y = -4721697;
nist_z = 4078625;

% Calculate NIST LLA
nist_lla = ecef2lla([nist_x nist_y nist_z]);
nist_lat = nist_lla(1);
nist_long = nist_lla(2);
nist_alt = nist_lla(3);

coord_table(1,:) = [nist_x nist_y nist_z nist_lat nist_long nist_alt];

% Predefine EQUA LLA
equa_lat = 0;
equa_long = nist_long;
equa_alt = 0;

% Calculate EQUA ECEF
equa_ecef = lla2ecef([equa_lat equa_long equa_alt]);
equa_x = equa_ecef(1);
equa_y = equa_ecef(2);
equa_z = equa_ecef(3);

coord_table(2,:) = [equa_x equa_y equa_z equa_lat equa_long equa_alt];

% Predefine 89NO LLA
no89_lat = 89;
no89_long = nist_long;
no89_alt = 0;

% Calculate 89N0 ECEF
no89_ecef = lla2ecef([no89_lat no89_long no89_alt]);
no89_x = no89_ecef(1);
no89_y = no89_ecef(2);
no89_z = no89_ecef(3);

% Table Printout
Site = {'NIST';'EQUA';'89NO'};
ECEF_X = {sprintf('%.0f',nist_x);sprintf('%.0f',equa_x);sprintf('%.0f',no89_x)};
ECEF_Y = {sprintf('%.0f',nist_y);sprintf('%.0f',equa_y);sprintf('%.0f',no89_y)};
ECEF_Z = {sprintf('%.0f',nist_z);sprintf('%.0f',equa_z);sprintf('%.0f',no89_z)};
Latitude = {sprintf('%.5f',nist_lat);sprintf('%.5f',equa_lat);sprintf('%.5f',no89_lat)};
Longitude = {sprintf('%.5f',nist_long);sprintf('%.5f',equa_long);sprintf('%.5f',no89_long)};
Altitude = {sprintf('%.0f',nist_alt);sprintf('%.0f',equa_alt);sprintf('%.0f',no89_alt)};

coord_table = table(Site,ECEF_X,ECEF_Y,ECEF_Z,Latitude,Longitude,Altitude);

%------------- END OF CODE --------------
