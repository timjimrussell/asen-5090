function visibility_table = plot_visible_sv(user_ecef, file)
%plot_visible_sv - Plot skyplots of visible SVs for a user at a given time
%
% Inputs:
%    user_ecef - 1x3 float, location of user in ECEF coordinates in m
%    file      - string, name of file to be scanned
%
% Outputs:
%    visibility_table - Report of visible SVs and their az/el/range
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

sv_ecef_matrix = extract_ecef(file);

sv = sv_ecef_matrix(:,1);
[az, el, rng] = calc_azelrange(user_ecef,sv_ecef_matrix(:,2:4));

PRN = {};
Azimuth = {};
Elevation = {};
Range = {};

num_sv = length(el);
j=1;
for i = 1:num_sv
    if el(i) > 0
        PRN = [PRN;sprintf('%d',sv(i))];
        Azimuth = [Azimuth;sprintf('%.5f',az(i))];
        Elevation = [Elevation;sprintf('%.5f',el(i))];
        Range = [Range;sprintf('%.0f',rng(i))];
        
        plotAzEl(az(i),el(i),sv(i))
    end
end

visibility_table = table(PRN,Azimuth,Elevation,Range);

%------------- END OF CODE --------------