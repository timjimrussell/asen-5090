function los_enu = calc_los(user_ecef, sv_ecef)
%calc_los - Calculate the unit LOS vector from a user to a space vehicle in 
%           ENU coordinates
%
% Inputs:
%    user_ecef  - 1x3 float, location of user in ECEF coordinates in m
%    sv_ecef    - 1x3 float, location of space vehicle in ECEF coordinates
%                            in m
%
% Outputs:
%    los_enu    - 1x3 float, line-of-sight unit vector from user to SV in m
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

% Find user latitude and longitude
user_lla = ecef2lla(user_ecef);
user_lat = user_lla(1);
user_long = user_lla(2);

% Find ECEF LOS vector and normalize
los_ecef = sv_ecef - user_ecef;
los_ecef = los_ecef./norm(los_ecef);

% Find ENU DCM at user location
dcm = dcm_ecef2enu(user_lat, user_long);

% Convert ECEF LOS vector to ENU
los_enu = (dcm*los_ecef.').';

%------------- END OF CODE --------------
