function dcm = dcm_ecef2enu(lat, long)
%dcm_ecef2enu - Generates a DCM to convert from ECEF to ENU coordinates
%
% Inputs:
%    lat  - float, latitude in deg
%    long - float, longitude in deg
%
% Outputs:
%    dcm - 3x3 float, direction cosine matrix
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

dcm = [          -sind(long),            cosd(long),         0;
       -sind(lat)*cosd(long), -sind(lat)*sind(long), cosd(lat);
        cosd(lat)*cosd(long),  cosd(lat)*sind(long), sind(lat)];

%------------- END OF CODE --------------
