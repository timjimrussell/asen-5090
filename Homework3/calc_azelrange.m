function [az, el, range] = calc_azelrange(user_ecef, sv_ecef)
%calc_azelrange - Calculate the azimuth angle, elevation angle, and range 
%                 from a user to space vehicles
%
% Inputs:
%    user_ecef  - 1x3 float, location of user in ECEF coordinates in m
%    sv_ecef    - nx3 float, location of space vehicles in ECEF coordinates
%                            in m
%
% Outputs:
%    az         - nx1 float, azimuth angle in deg
%    el         - nx1 float, elevation angle in deg
%    range      - nx1 float, range to SV in m
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

% Grab number of SVs to iterate over
[num_sv, ~] = size(sv_ecef);

% Initialize output
az = zeros(num_sv,1);
el = az;
range = az;

% Loop through each SV
for i = 1:num_sv
    % Calculate range
    range(i,1) = norm(sv_ecef(i,:)-user_ecef);
    
    % Calculate ENU coordinates
    los_enu = calc_los(user_ecef, sv_ecef(i,:));
    E = los_enu(1);
    N = los_enu(2);
    U = los_enu(3);
     
    % Calculate azimuth and elevation
    az(i,1) = atan2d(E,N);
    el(i,1) = asind(U/sqrt(E^2 + N^2 + U^2));
end

%------------- END OF CODE --------------
