function sv_ecef_matrix = extract_ecef(file)
%extract_ecef - Parse a text file to retrieve ECEF coordinates for all SVs
%
% Inputs:
%    file - string, name of file to be scanned
%
% Outputs:
%    sv_ecef_matrix - 32x4 matrix with PRN and ECEF coordinates of each SV
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    ASEN 5090 Lecture Material, Axelrad, Akos, Larson, & Morton
%
% 2020-09-22 - Initial version
%------------- BEGIN CODE --------------

fid = fopen(file,'r');

i = 1;

line = fgetl(fid);
while line ~= -1
    line_split = strsplit(line);
    PRN(i) = str2double(line_split(1));
    ECEF_X(i) = str2double(line_split(2))*1e3;
    ECEF_Y(i) = str2double(line_split(3))*1e3;
    ECEF_Z(i) = str2double(line_split(4))*1e3;
    i = i + 1;
    line = fgetl(fid);
end

sv_ecef_matrix = [PRN.' ECEF_X.' ECEF_Y.' ECEF_Z.'];

%------------- END OF CODE --------------

    
    
