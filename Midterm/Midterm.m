%% Problem 1
% Part A

close all
clear

% Define timespan
t = -86400/2:86400/2; % One day, centered on EQUA pass

% Define physical constants
mu = 3.98600e14; % standard gravitation of Earth, m^3/s^s
Omega_dot_E = 0.0042; % rotation rate of Earth, deg/s

% Define orbital elements
e = 0; % eccentricity, unitless
i = 55; % inclination, deg
om = 0; % argument of perigee, deg
a = 26560e3; % semi-major axis, m
Omega_0 = 0; % RAAN, deg
n = sqrt(mu/a^3)*180/pi; % mean motion, deg/s
theta_0 = 0; % original true anomaly, deg

% Define User ECEF locations
user_ecef_equa = [6380e3 0 0];
user_ecef_nopo = [0 0 6380e3];

% Initialize output arrays
az_array_equa = [];
az_array_nopo = [];
el_array_equa = [];
el_array_nopo = [];
rng_array_equa = [];
rng_array_nopo = [];
t_array_equa = [];
t_array_nopo = [];

for j = 1:86401
    % Calculate varying orbit elements
    theta = theta_0 + n*t(j); % Propagate true anomaly
    x_pri = a*cosd(theta); % X-coordinate in orbit plane
    y_pri = a*sind(theta); % Y-coordinate in orbit plane
    Omega = Omega_0 - Omega_dot_E*t(j); % Calc arg of long
    % Calculate ECEF coordinates
    ECEF_x(j) = x_pri*cosd(Omega)-y_pri*cosd(i)*sind(Omega);
    ECEF_y(j) = x_pri*sind(Omega)+y_pri*cosd(i)*cosd(Omega);
    ECEF_z(j) = y_pri*sind(i);
    % Get az/el/range for each site
    [az_equa, el_equa, rng_equa] = calc_azelrange(user_ecef_equa, [ECEF_x(j) ECEF_y(j) ECEF_z(j)]);
    [az_nopo, el_nopo, rng_nopo] = calc_azelrange(user_ecef_nopo, [ECEF_x(j) ECEF_y(j) ECEF_z(j)]);
    % Update output arrays if visible
    if el_equa >= 0
        az_array_equa = [az_array_equa; az_equa];
        el_array_equa = [el_array_equa; el_equa];
        rng_array_equa = [rng_array_equa; rng_equa];
        t_array_equa = [t_array_equa; t(j)];
    end
    if el_nopo >= 0
        az_array_nopo = [az_array_nopo; az_nopo];
        el_array_nopo = [el_array_nopo; el_nopo];
        rng_array_nopo = [rng_array_nopo; rng_nopo];
        t_array_nopo = [t_array_nopo; t(j)];
    end
end

% SV arrays for skyplot
sv_array_equa = zeros(length(t_array_equa),1);
sv_array_nopo = zeros(length(t_array_nopo),1);

% Plots for equator
figure
scatter(t_array_equa,el_array_equa,'.')
title('Equator Site Elevation Measurements')
xlabel('Time (s)')
ylabel('Elevation Angle (deg)')
figure
scatter(t_array_equa,rng_array_equa,'.')
title('Equator Site Range Measurements')
xlabel('Time (s)')
ylabel('Range (m)')
figure
plotAzEl(az_array_equa,el_array_equa,sv_array_equa)
title('Equator Site Sky Plot')

% Plots for North Pole
figure
scatter(t_array_nopo,el_array_nopo,'.')
title('North Pole Site Elevation Measurements')
xlabel('Time (s)')
ylabel('Elevation Angle (deg)')
figure
scatter(t_array_nopo,rng_array_nopo,'.')
title('North Pole Site Range Measurements')
xlabel('Time (s)')
ylabel('Range (m)')
figure
plotAzEl(az_array_nopo,el_array_nopo,sv_array_nopo)
title('North Pole Site Sky Plot')

% Part B
min_rng_equa = min(rng_array_equa)
min_rng_nopo = min(rng_array_nopo)

% Part C
max_rng_equa = rng_array_equa(find(el_array_equa==min(el_array_equa)))
max_rng_nopo = rng_array_nopo(find(el_array_nopo==min(el_array_nopo)))

% Part E
max_el_equa = max(el_array_equa)
max_el_nopo = max(el_array_nopo)

% Part F
pass_length_equa = (t_array_equa(26603)-t_array_equa(1))/3600
pass_length_nopo = (t_array_nopo(17458)-t_array_nopo(1))/3600

% Part G
rng_rate_equa = diff(rng_array_equa)./diff(t_array_equa);
rng_rate_nopo = diff(rng_array_nopo)./diff(t_array_nopo);

L1 = 1575.42e6; % MHz
c = 3e8;

doppler_shift_equa = L1*rng_rate_equa./c;
doppler_shift_nopo = L1*rng_rate_nopo./c;

min_doppler_shift_equa = min(abs(doppler_shift_equa))
min_doppler_shift_nopo = min(abs(doppler_shift_nopo))

% Part H
max_doppler_shift_equa = max(abs(doppler_shift_equa))
max_doppler_shift_nopo = max(abs(doppler_shift_nopo))

%% Problem 3A
A = [1 1 -1 1 -1 1 -1 1 1 -1 -1 -1];
A_RX = [-1 -1 -1 1 1 -1 1 -1 1 -1 1 1];
cross_corr = zeros(1,12);

for i = 1:12
    A_shift = [A(i+1:12) A(1:i)];
    cross_corr(i) = A_RX*A_shift.';
end
