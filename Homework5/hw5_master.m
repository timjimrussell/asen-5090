function hw5_master
%% Setup

clear
close all

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework2'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

%% Problem 1

% Define NIST ECEF coordinates
nist_ecef = [-1288398.360 -4721697.040 4078625.500];

% Build PRN07 data from RINEX
prn07_nist_meas = read_rinex_obs8('nist2450.20o',30);
weektimes = prn07_nist_meas.data(:,1:2);
hours = (weektimes(:,2)-86400*2)./3600;
prn07_pseudo_C1 = prn07_nist_meas.data(:,4);

% Build PRN07 expected range data from ephemeris
ephem = read_clean_GPSbroadcast('brdc2450.20n');
[~, prn07_ecef_ephem] = broadcast_eph2pos(ephem, weektimes,30);
prn07_range_corr = zeros(length(hours),1);
for i = 1:length(hours)
    prn07_range_corr(i,1) = correct_range(nist_ecef,prn07_ecef_ephem(i,:),ephem,weektimes(i,2),30);
end

dPR0 = prn07_pseudo_C1 - prn07_range_corr;

figure
plot(hours,dPR0)

%% Problem 2

% Calculate SV clock error
bsv = error_svclock(ephem,weektimes,30);

dPR1 = prn07_pseudo_C1 - (prn07_range_corr - bsv);

figure
plot(hours,dPR1)

%% Problem 3

% Calculate relativity error
relsv = error_relativity(ephem,weektimes,30);

dPR2 = prn07_pseudo_C1 - (prn07_range_corr - bsv - relsv);

figure
plot(hours,dPR2)

%% Problem 4

% Calculate tropophere error
T_zd = 2; % m, assumed
tropo = error_tropo(nist_ecef,prn07_ecef_ephem,T_zd); % figure out how to use corrected SV ecef

dPR3 = prn07_pseudo_C1 - (prn07_range_corr - bsv - relsv + tropo);

figure
plot(hours,dPR3)

%% Problem 5

% Define L2 phase pseudorange
prn07_pseudo_P2 = prn07_nist_meas.data(:,8);

% Define frequencies
f1 = 1575.42e6;
f2 = 1227.60e6;

% Calculate ionosphere-free pseudorange and ionosphere delays
[PRIF, iono] = error_iono(prn07_pseudo_C1,f1,prn07_pseudo_P2,f2);

dPR4 = PRIF - (prn07_range_corr - bsv - relsv + tropo);

figure
plot(hours,dPR4)

%% Problem 6

figure
hold on
scatter(hours,dPR1,'.')
scatter(hours,dPR2,'.')
scatter(hours,dPR3,'.')
scatter(hours,dPR4,'.')

