function bsv = error_svclock(ephem, weektime, prn)

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Define physical constants
c = 299792458; % m/s

% Size time and position
[num_meas, ~ ] = size(weektime);
bsv = zeros(num_meas,1);

% Filter and size ephemeris by PRN number
ephem_filtered = ephem(find(ephem(:,1)==prn),:);
[num_entries, ~] = size(ephem_filtered);

for i = 1:num_meas
    % Grab correct ephemeris entry
    use_index = 0;
    week = weektime(i,1);
    tow = weektime(i,2);
    for j = 1:num_entries
        if tow >= ephem_filtered(j,17)
            use_index = j;
        else
            break
        end
    end
    % In case time stamp is before first entry, use first entry and warn
    if use_index == 0
        use_index = 1;
        fprintf('WARNING: Time stamp comes before first ephem entry!\n')
    end
    
    % Calculate time from reference
    t_e = ephem_filtered(use_index,17); % s - time into week at ephem entry
    del_t = tow - t_e; % s - time into week at measurement
    
    % Extract time coefficients
    af0 = ephem_filtered(use_index,21);
    af1 = ephem_filtered(use_index,22);
    af2 = ephem_filtered(use_index,23);
    
    % Calculate time bias
    bsv(i,1) = c*(af0 + af1*del_t + af2*del_t^2);
end
