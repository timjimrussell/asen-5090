function [PRIF,iono] = error_iono(C1,f1,P2,f2)

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Calculate TEC
TEC = f1^2*f2^2/(40.3*(f1^2-f2^2)*(P2-C1));

% Calculate iono delays
I1 = 40.3*TEC./f1^2;
I2 = 40.3*TEC./f2^2;
iono = [I1 I2];

% Calculate iono-free pseudorange
PRIF = (f1^2*C1 - f2^2*P2)./(f1^2 - f2^2);
