function tropo = error_tropo(user_ecef,sv_ecef,T_zd)

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Get elevation from user to SV for each measurement
[~, el, ~] = calc_azelrange(user_ecef,sv_ecef);

% Calculate troposphere delays based on zenith estimate and obliquity factor
tropo = T_zd./sind(el);
