function rel = error_relativity(ephem, weektime, prn)

addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework3'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\data'
addpath 'C:\Users\timji\Documents\School\Colorado\ASEN5090\asen-5090\Homework4\support_scripts'

% Define physical constants
c = 299792458; % m/s
mu = 3.986005e14; % m^3/s^2

% Size time and position
[num_meas, ~ ] = size(weektime);
rel = zeros(num_meas,1);

% Filter and size ephemeris by PRN number
ephem_filtered = ephem(find(ephem(:,1)==prn),:);
[num_entries, ~] = size(ephem_filtered);

for i = 1:num_meas
    % Grab correct ephemeris entry
    use_index = 0;
    week = weektime(i,1);
    tow = weektime(i,2);
    for j = 1:num_entries
        if tow >= ephem_filtered(j,17)
            use_index = j;
        else
            break
        end
    end
    % In case time stamp is before first entry, use first entry and warn
    if use_index == 0
        use_index = 1;
        fprintf('WARNING: Time stamp comes before first ephem entry!\n')
    end

    % Calculate mean motion
    a = ephem_filtered(use_index,5)^2; % m - semi-major axis
    n0 = sqrt(mu/a^3); % rad/sec - mean motion

    % Calculate time from reference
    t_e = ephem_filtered(use_index,17); % s - time into week at ephem entry
    t_j = tow - t_e; % s - time into week at measurement

    % Calculate corrected mean motion
    deln = ephem_filtered(use_index,3); % rad/s - mean motion correction
    n = n0 + deln; % rad/s - corrected mean motion

    % Calculate mean anomaly
    M_0 = ephem_filtered(use_index,2); % rad - mean anomaly at ephem entry
    M_j = M_0 + n*t_j; % rad - mean anomaly at measurement

    % Calculate eccentric anomaly
    e = ephem_filtered(use_index,4); % unitless - eccentricity of orbit
    E_j = newton_kepler_solver(M_j, a, e, 1e-2);
    
    rel(i,1) = -2/c*sqrt(mu*a)*e*sin(E_j);
end