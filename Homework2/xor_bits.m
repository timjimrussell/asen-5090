function result = xor_bits(bit_array)
%XOR_BITS - Performs a bitwise logical XOR for a bit array
%
% Inputs:
%    bit_array - 1xn Int, an array of bits
%
% Outputs:
%    results - Int, logical XOR of bit_array
%
% Assumptions:
%    Array is solely populated with 0 or 1 values
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

% Logical XOR equivalent to MOD2 of sum of bits
result = mod(sum(bit_array),2);

%------------- END OF CODE --------------