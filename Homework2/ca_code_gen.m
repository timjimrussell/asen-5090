function ca_code = ca_code_gen(code_num, epoch1, epoch2)
%CA_CODE_GEN - Generates the C/A code for a GPS PRN number.
%
% Inputs:
%    code_num - Int, GPS PRN signal number
%
% Outputs:
%    ca_code - 1xn Int, C/A code sequence
%    epoch1 - Int, first epoch to be output
%    epoch2 - Int, last epoch to be output
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

% Initialize output code array
ca_code = ones(1, epoch2-epoch1+1);

% Initialize shift registers to all 1's
G1_reg = reg_init(10);
G2_reg = reg_init(10);

% Determine phase selector bits
[ps_bit1,ps_bit2] = phase_selector_bits(code_num);

% Shift G1 and G2 registers correctly until first epoch is reached
j = 1;
while j < epoch1
    % Perform appropriate XOR for next G1 bit
    G1_in = xor_bits([G1_reg(3) G1_reg(10)]);
    % Shift G1
    [~, G1_reg] = reg_shift(G1_in, G1_reg);
    
    % Perform appropriate XOR for next G2 bit
    G2_in = xor_bits([G2_reg(2) G2_reg(3) G2_reg(6) G2_reg(8) G2_reg(9) ...
                      G2_reg(10)]);
    % Shift G2
    [~, G2_reg] = reg_shift(G2_in, G2_reg);
    
    j = j+1;
end

for i = 1:(epoch2-epoch1+1)
    % Perform appropriate XOR for next G1 bit
    G1_in = xor_bits([G1_reg(3) G1_reg(10)]);
    % Shift G1
    [G1_out, G1_reg] = reg_shift(G1_in, G1_reg);
    
    % Calculate output of G2 phase selector
    G2i_out = xor_bits([G2_reg(ps_bit1) G2_reg(ps_bit2)]);

    % Calculate next CA code bit
    ca_code(i) = xor_bits([G1_out G2i_out]);
    
    % Perform appropriate XOR for next G2 bit
    G2_in = xor_bits([G2_reg(2) G2_reg(3) G2_reg(6) G2_reg(8) G2_reg(9) ...
                      G2_reg(10)]);
    % Shift G2
    [~, G2_reg] = reg_shift(G2_in, G2_reg);

end

%------------- END OF CODE --------------