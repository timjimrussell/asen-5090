function [out_bit, new_reg] = reg_shift(in_bit, old_reg)
%REG_SHIFT - Shifts a register one bit, collects the output bit, and 
%            inserts the new bit
%
% Inputs:
%    in_bit - Int, the bit to be insterted into the LSB of the register
%    old_reg - 1xn Int, the register to be shifted
%
% Outputs:
%    out_bit - Int, the bit which was output
%    results - 1xn Int, the register following the shift
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

reg_len = length(old_reg);

% Snag last bit as the output bit
out_bit = old_reg(reg_len);

% Shift right and insert new bit
new_reg = [in_bit old_reg(1:reg_len-1)];

%------------- END OF CODE --------------