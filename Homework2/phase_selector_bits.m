function [bit1,bit2] = phase_selector_bits(code_num)
%PHASE_SELECTOR_BITS - Returns the bits used in the phase selector for a 
%                      given GPS PRN code
%
% Inputs:
%    code_num - Int, GPS PRN signal number
%
% Outputs:
%    bit1 - Int, LSB used for phase selector
%    bit2 - Int, MSB used for phase selector
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    NAVSTAR GPS IS-GPS-200 Rev D
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

switch code_num
    case 1
        bit1 = 2;
        bit2 = 6;
    case 2
        bit1 = 3;
        bit2 = 7;
    case 3
        bit1 = 4;
        bit2 = 8;
    case 4
        bit1 = 5;
        bit2 = 9;
    case 5
        bit1 = 1;
        bit2 = 9;
    case 6
        bit1 = 2;
        bit2 = 10;
    case 7
        bit1 = 1;
        bit2 = 8;
    case 8
        bit1 = 2;
        bit2 = 9;
    case 9
        bit1 = 3;
        bit2 = 10;
    case 10
        bit1 = 2;
        bit2 = 3;
    case 11
        bit1 = 3;
        bit2 = 4;
    case 12
        bit1 = 5;
        bit2 = 6;
    case 13
        bit1 = 6;
        bit2 = 7;
    case 14
        bit1 = 7;
        bit2 = 8;
    case 15
        bit1 = 8;
        bit2 = 9;
    case 16
        bit1 = 9;
        bit2 = 10;
    case 17
        bit1 = 1;
        bit2 = 4;
    case 18
        bit1 = 2;
        bit2 = 5;
    case 19
        bit1 = 3;
        bit2 = 6;
    case 20
        bit1 = 4;
        bit2 = 7;
    case 21
        bit1 = 5;
        bit2 = 8;
    case 22
        bit1 = 6;
        bit2 = 9;
    case 23
        bit1 = 1;
        bit2 = 3;
    case 24
        bit1 = 4;
        bit2 = 6;
    case 25
        bit1 = 5;
        bit2 = 7;
    case 26
        bit1 = 6;
        bit2 = 8;
    case 27
        bit1 = 7;
        bit2 = 9;
    case 28
        bit1 = 8;
        bit2 = 10;
    case 29
        bit1 = 1;
        bit2 = 6;
    case 30
        bit1 = 2;
        bit2 = 7;
    case 31
        bit1 = 3;
        bit2 = 8;
    case 32
        bit1 = 4;
        bit2 = 9;
    case 33
        bit1 = 5;
        bit2 = 10;
    case 34
        bit1 = 4;
        bit2 = 10;
    case 35
        bit1 = 1;
        bit2 = 7;
    case 36
        bit1 = 2;
        bit2 = 8;
    case 37
        bit1 = 4;
        bit2 = 10;
end        

%------------- END OF CODE --------------
