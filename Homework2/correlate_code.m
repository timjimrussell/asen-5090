function corr_val = correlate_code(code1,code2,convert)
%CORRELATE_CODE - Correlates two shifted C/A code sequences
%
% Inputs:
%    code1 - Int, PRN code designation for first C/A code sequence
%    code2 - Int, PRN code designation for second C/A code sequence
%
% Outputs:
%    corr_val - Int, normalized correlation value between the two codes
%
% Assumptions:
%    Both codes are full length, i.e. 1023 chips
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

corr_val = 0;

% Make sure code is in proper format if flag set
if convert == 1
    code1 = zeroone2negplus(code1);
    code2 = zeroone2negplus(code2);
end

for i = 1:1023
    corr_val = corr_val + code1(i)*code2(i);
end

corr_val = corr_val/1023;

%------------- END OF CODE --------------