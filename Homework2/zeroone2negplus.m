function new_code = zeroone2negplus(old_code)
%ZEROONE2NEGPLUS - Convert C/A code from 0/1 to -1/1
%
% Inputs:
%    old_code - 1xn Int, C/A code to be converted
%
% Outputs:
%    new_code - 1xn Int, C/A code that was converted
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

code_len = length(old_code);
new_code = old_code;

for i = 1:code_len
    if new_code(i) == 0
        new_code(i) = -1;
    end
end

%------------- END OF CODE --------------