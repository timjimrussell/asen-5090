function register = reg_init(n)
%REG_INIT - Initializes a phase register for C/A code generation
%
% Inputs:
%
% Outputs:
%    register - 1xn Int, the initialized register
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

register = ones(1,n);

%------------- END OF CODE --------------