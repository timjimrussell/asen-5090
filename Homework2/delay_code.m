function delayed_code = delay_code(code, delay)
%DELAY_CODE - Delays a C/A code by a number of chips
%
% Inputs:
%    code - 1x1023 Int, C/A code to be delayed
%    delay - Int, number of chips to delay
%
% Outputs:
%    delayed_code - 1xn Int, resulting delayed code
%
% Author: Tim Russell
% Graduate Student, University of Colorado
% Boulder, CO
% email: timothy.russell@colorado.edu
%
% References:
%    Global Positioning System, Misra & Enge
%
% 2020-09-09 - Initial version
%------------- BEGIN CODE --------------

code_len = length(code);
delayed_code = [code(delay+1:code_len) code(1:delay)];

%------------- END OF CODE --------------